import './App.css';
import { CardComponent } from './components/card/cardComponent';

function App() {
  return (
    <div className='cardContainer'>
      <CardComponent title={'Div 1'} style={{ backgroundColor: '#4cdc14' }}></CardComponent>
      <CardComponent title={'Div 2'} style={{ backgroundColor: '#2e54fc' }}></CardComponent>
    </div>
  );
}

export default App;
