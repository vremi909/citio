import './cardComponentStyles.css';

export const CardComponent = (props) => {
    return (
        <div className="card" style={props.style}>
            {props.title}
        </div>
    )
}